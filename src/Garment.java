
public class Garment {
	private boolean dirty;
	
	private String ownerTaxId;		
	
	public Garment(String ownerTaxId)
	{
		this.ownerTaxId = ownerTaxId;
		dirty = true;
	}
	
	public String getOwnerTaxId() {
		return ownerTaxId;
	}
	
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}
}
