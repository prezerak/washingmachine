
public class WashingMachine {
	boolean washing=false;
	
	public void start(Basket b)
	{
		if (washing)
		{
			System.out.println("Machine in use....");
			return;
		}
		
		washing = true;		
		this.wash(b.getDirtyGarments());
	}
	
	private void wash(Garment[] garments)
	{
		//washing....
		for (Garment g: garments)
		{
			g.setDirty(false);
		}
		washing = false;
	}
}
