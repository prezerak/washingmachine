
public class WashingApp {
	
	public static void main(String[] args) {
		
		
		Person mitsos = new Person("Dimitrios", "Papadopoulos", "023349494", 30);			
		
		Person thanassis = new Person("Athanassios", "Zafeiratos", "685940302");
		
		Garment tShirt = new Garment(mitsos.getTaxId());
		Garment jeans = new Garment(mitsos.getTaxId());
		
		Garment jeans2 = new Garment(thanassis.getTaxId());
		
		mitsos.addGarmentToBasket(tShirt);
		mitsos.addGarmentToBasket(jeans);
		
		thanassis.addGarmentToBasket(jeans2);
		
				
		System.out.println(mitsos.getAge());
		System.out.println(thanassis.getLastName());
		System.out.println(mitsos.getFirstName());	
		
		
		WashingMachine washingMachine = new WashingMachine();
		washingMachine.start(mitsos.getBasket());		
		washingMachine.start(thanassis.getBasket());
				
		
		
		
		
		//1.000.000 lines of code afterwards
		thanassis.setAge(50);
	}
}
