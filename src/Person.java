
public class Person {
	
	private String taxId; 
	
	private String firstName;	
	private String lastName;
	private int age;
	private Basket basket;
	
	
	

	public Person(String firstName, String lastName)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.basket = new Basket();
	}
	
	
	public Person(String firstName, String lastName, String taxId, int age)
	{
		this(firstName,lastName);				
		this.age = age;
		this.taxId = taxId;
	}
	
	public Person(String firstName, String lastName, String taxId)
	{
		this(firstName,lastName);						
		this.taxId = taxId;
	}
	
	public Basket getBasket() {
		return basket;
	}


	public String getFirstName() {
		return this.firstName;
	}
	
	
	public String getLastName() {
		return this.lastName;
	}
	
	//////
	
	public int getAge() {
		return this.age;
	}
	
	
	
	public void setAge(int age) {
		this.age = age;
	}
	
	
	public void addGarmentToBasket(Garment g)
	{
		if (g.getOwnerTaxId() != this.taxId )
		{
			System.out.println("This garment does not belong to " + this.lastName);
			return;
		}
		
  	    this.basket.add(g);
	}


	public void emptyBasket() {
		this.basket.empty();		
	}


	public String getTaxId() {
		return this.taxId;
	}
}
