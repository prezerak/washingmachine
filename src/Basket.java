
public class Basket {
	
	final int CAPACITY = 10; 
	private Garment [] dirtyGarments = new Garment[CAPACITY];
	

	private int position = 0;
	
	public int add(Garment g) {
											
		if (position==CAPACITY)
		{
			return position;
		}
		dirtyGarments[position++] = g;
		
		return position;
	}
	
	public void empty()
	{
		position=0;
	}
	
	public Garment[] getDirtyGarments() {
		position=0;
		return dirtyGarments;
	}
	
	

}
